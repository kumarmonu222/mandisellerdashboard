import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SellerJwtInterceptor } from './seller-jwt-interceptor';

export const httpInterceptorProviders = [
    { provide: HTTP_INTERCEPTORS, useClass: SellerJwtInterceptor, multi: true }
]