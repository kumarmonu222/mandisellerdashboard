import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';
import { ScrollingModule } from '@angular/cdk/scrolling';
import {MatButtonModule, MatCheckboxModule, MatNativeDateModule, MatDatepickerModule, MatTableModule, MatToolbarModule, MatIconModule, MatSidenavModule, MatListModule, MatProgressSpinnerModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatTooltipModule, MatDialogModule, MatCardModule, MatSortModule, MatAutocompleteModule} from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';

const MaterialComponents = [
  MatToolbarModule,
  MatSidenavModule,
  ScrollingModule,
  MatProgressSpinnerModule,
  LayoutModule,
  MatDialogModule,
  MatIconModule,
  MatMenuModule,
  MatPaginatorModule,
  MatFormFieldModule,
  MatInputModule,
  MatTooltipModule,
  MatCardModule,
  MatAutocompleteModule,
  MatButtonModule,
  MatTableModule,
  MatListModule,
  MatSortModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCheckboxModule,
  //other whole page applicable modules
]

@NgModule({
  declarations: [],
  imports: [ 
    MaterialComponents
  ],
  exports: [ MaterialComponents ]
})
export class AngularMaterialModule { }