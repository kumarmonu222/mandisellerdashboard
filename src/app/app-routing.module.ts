import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './_components/pagenotfound/pagenotfound.component';
import { SellerGuard } from './_guards/seller.guard';
import { ProfileComponent } from './_components/profile/profile.component';
import { LoginComponent } from './_components/login/login.component';
import { NavbarComponent } from './_components/navbar/navbar.component';
import { DashboardComponent } from './_components/dashboard/dashboard.component';
import { ShopComponent } from './_components/shop/shop.component';
import { BijakComponent } from './_components/bijak/bijak.component';
import { AllBijakComponent } from './_components/all-bijak/all-bijak.component';
import { AddBijakComponent } from './_components/add-bijak/add-bijak.component';
import { EditBijakComponent } from './_components/edit-bijak/edit-bijak.component';
import { SellReportComponent } from './_components/sell-report/sell-report.component';
import { CustomerComponent } from './_components/customer/customer.component';
import { BillComponent } from './_components/bill/bill.component';
import { SingleCustomerComponent } from './_components/single-customer/single-customer.component';
import { MerchantComponent } from './_components/merchant/merchant.component';
import { SingleMerchantComponent } from './_components/single-merchant/single-merchant.component';

const routes: Routes = [
  {path: '', component:LoginComponent},
  {path: 'dashboard', component:NavbarComponent,
    canActivate: [SellerGuard],
    children: [
      {path:'', component: DashboardComponent},
      {path:'home', component: DashboardComponent},
      {path:'profile', component: ProfileComponent},
      {path:'shop', component: ShopComponent},
      {path:'shop', component: ShopComponent},
      {path:'customer', component: CustomerComponent},
      {path:'customer/:customerName', component: SingleCustomerComponent},
      {path:'merchant', component: MerchantComponent},
      {path:'merchant/:merchantName', component: SingleMerchantComponent},
      {path:'bill', component: BillComponent},
      {path:'bijak', component: BijakComponent,
        children: [
          { path: '', redirectTo: 'all-bijak', pathMatch: 'full' },
          { path: 'all-bijak', component: AllBijakComponent },
          { path:'edit-bijak', component: EditBijakComponent },
          { path:'add-bijak', component: AddBijakComponent },
        ]
      },
      {path:'sell-report', component: SellReportComponent},
    ]
  },
  {path: '**', component: PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
