import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import * as jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  shopId: any;
  customers: any;
  totalAmountPending: number = 0;
  totalCaratsPending: number = 0;
  totalCustomers: any;
  totalAmtPendings: any;
  totalCrtsPendings: any;
  merchants: any;
  totalMerchants: any;
  salesReportData: any;
  todayDate: any;

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private router:Router,
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.todayDate = this.formatDate(new Date());
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopId = data.shop._id;
      this.commonServ.post('api/bill/specific-shop-bills/sales-report-for-specific-day/' + this.shopId, {searchDate: this.todayDate}).subscribe((data: any) =>{
        this.salesReportData = data.data[0];
      });
      this.commonServ.getAll('api/merchant/specific-shop-merchants/' + this.shopId).subscribe((data: any) =>{
        this.totalMerchants = data.count;
      });
      this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=1&limit=800').subscribe((data: any) =>{
        this.customers = data.data;
        this.totalCustomers = data.pagination.total;
        if(this.customers.length) {
          this.customers.map(customer => {
            this.totalAmountPending += customer.totalBillAmount - customer.totalDeposit;
            this.totalCaratsPending += customer.totalCaratsRemaining;
          });
        }
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    });
  }

  checkList() {
    this.router.navigateByUrl('dashboard/customer');
  }

  checkPartyList() {
    this.router.navigateByUrl('dashboard/merchant');
  }

  checkAmountPending() {
    this.spinner.show();
    this.commonServ.getAll('api/customer/specific-shop-customers/amount-pending/' + this.shopId).subscribe((data: any) =>{
      this.totalAmtPendings = data.data;
      this.spinner.hide();
    }, (error:any) => {
      
    });
  }

  checkCaratPending() {
    this.spinner.show();
    this.commonServ.getAll('api/customer/specific-shop-customers/carat-pending/' + this.shopId).subscribe((data: any) =>{
      this.totalCrtsPendings = data.data;
      this.spinner.hide();
    }, (error:any) => {
      
    });
  }

  salesReportForSpecificDate(event) {
    this.commonServ.post('api/bill/specific-shop-bills/sales-report-for-specific-day/' + this.shopId, {searchDate: event.target.value}).subscribe((data: any) =>{
      this.salesReportData = data.data[0];
    });
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  }
}
