import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import * as jwt_decode from 'jwt-decode';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-merchant',
  templateUrl: './merchant.component.html',
  styleUrls: ['./merchant.component.scss']
})
export class MerchantComponent implements OnInit {
  @ViewChild('closebutton', { static: true }) closebutton;
  @ViewChild('closeeditbutton', { static: true }) closeeditbutton;
  shopId: any;
  merchants: any;
  addMerchantForm: FormGroup;
  editMerchantForm: FormGroup;
  submitted: boolean;
  merchant: any;
  cities: string[] = [];
  filteredCityOptions: Observable<string[]>;
  AllCities: any = [];

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { 
    this.addMerchantForm = this.formbuilder.group({ 
      city: new FormControl(name, Validators.required),
      name: ['', [Validators.required]],
      phone: [''],
    });

    this.editMerchantForm = this.formbuilder.group({ 
      phone: [''],
    });
  }
  
  private _filterCity(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.cities.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit() {
    this.spinner.show();
    this.filteredCityOptions = this.addMerchantForm.get('city').valueChanges.pipe(
      startWith(''),
      map(value => this._filterCity(value))
    );
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopId = data.shop._id;
      this.commonServ.getAll('api/merchant/specific-shop-merchants/' + this.shopId).subscribe((data: any) =>{
        this.merchants = data.merchant;
        data.merchant.forEach(merchant => {
          this.AllCities.push(merchant.city);
        });
        this.cities = Array.from(new Set(this.AllCities));
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    });
  }

  onReset() {
    this.submitted = false;
    this.addMerchantForm.reset();
    this.editMerchantForm.reset();
  }

  onAddMerchantSubmit() {
    this.submitted = true;
    this.addMerchantForm.value.shopId = this.shopId;
    this.spinner.show();
    this.commonServ.post('api/merchant', this.addMerchantForm.value).subscribe((data: any) => {
      this.toastr.success('Merchant creted successfully');
      this.closebutton.nativeElement.click();
      this.commonServ.getAll('api/merchant/specific-shop-merchants/' + this.shopId).subscribe((data: any) =>{
        this.merchants = data.merchant;data.merchant.forEach(merchant => {
          this.AllCities.push(merchant.city);
        });
        this.cities = Array.from(new Set(this.AllCities));
        this.spinner.hide();
      });
    }, (error) => {
      console.log(error);
    });
  }

  deleteMerchant(id, name, city) {
    if (confirm(`Sure to delete ${name.toUpperCase()} | ${city.toUpperCase()}`)) {
      this.spinner.show();
      this.commonServ.delete('api/merchant/delete', id).subscribe((data: any) => {
        this.toastr.success('Merchant deleted successfully');
        this.commonServ.getAll('api/merchant/specific-shop-merchants/' + this.shopId).subscribe((data: any) =>{
          this.merchants = data.merchant;
          this.spinner.hide();
        });
      }, (error) => {
        console.log(error);
      });
    }
  }

  getSingleMerchant(id) {
    this.spinner.show();
    this.commonServ.getSingle('api/merchant', id).subscribe((data: any) =>{
      this.spinner.hide();
      this.merchant = data.merchant;
    });
  }

  onEditMerchantSubmit(id) {
    this.submitted = true;
    this.editMerchantForm.value.shopId = this.shopId;
    this.spinner.show();
    this.commonServ.update('api/merchant/update', id, this.editMerchantForm.value).subscribe((data: any) => {
      this.toastr.success('Merchant updated successfully');
      this.closeeditbutton.nativeElement.click();
      this.commonServ.getAll('api/merchant/specific-shop-merchants/' + this.shopId).subscribe((data: any) =>{
        this.merchants = data.merchant;
        this.spinner.hide();
      });
    }, (error) => {
      console.log(error);
    });
  }

  setMerchant(id, name, city, phone) {
    localStorage.setItem('merc_id', id);
    localStorage.setItem('merc_name', name);
    localStorage.setItem('merc_city', city);
    localStorage.setItem('merc_phone', phone);
  }
}