import { Component, OnInit, ViewChild, Inject, PLATFORM_ID } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from '@angular/forms';
import { Meta, Title, DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import * as jwt_decode from 'jwt-decode';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';
import * as $ from 'jquery';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})
export class BillComponent implements OnInit {
  @Inject(PLATFORM_ID) private platformId: Object;
  @ViewChild('closebutton', { static: true }) closebutton;
  @ViewChild('closeeditbutton', { static: true }) closeeditbutton;
  page: number = 1;
  itemPage: number = 10;
  shopId: any;
  bills: any;
  addBillForm: FormGroup;
  editBillForm: FormGroup;
  bill: any;
  itemRows: FormArray;
  selected: any;
  fromDatePicker: boolean = false;
  shopDetails: any;
  customerDetails: any;
  merchants: any;
  pagination: any;
  totalBills: any;
  customers: any;
  editCustName: any;
  editBillData: any;
  selected_customer: any = '';
  selected_date: any = '';
  billing_date: any = '';
  selected_city: any = '';
  options: string[] = [];
  cities: string[] = [];
  filteredOptions: Observable<string[]>;
  filteredCityOptions: Observable<string[]>;
  AllCities: any = [];
  AllCustomers: any = [];
  AllDescriptions: any = [];
  smallDevice: boolean;
  descriptions: any = [];
  todayDate: any;

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    public meta: Meta, public title: Title,
    private sanitizer: DomSanitizer,
    private spinner: NgxSpinnerService
  ) { 
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0, 0);
    }
    this.title.setTitle('Bill');
    this.meta.updateTag({ name: 'title', content: 'Bill' });
    if (window.outerWidth <= 768) {
      this.smallDevice = true;
    } else {
      this.smallDevice = false;
    }
    this.addBillForm = this.formbuilder.group({ 
      name: new FormControl(name, Validators.required),
      city: new FormControl(name, Validators.required),
      deposit: [''],
      discount: [''],
      caratsSent: [''],
      caratsRecieved: [''],
      itemRows: this.formbuilder.array([ this.createItem() ])
    });
    this.editBillForm = this.formbuilder.group({ 
      name: ['', [Validators.required]],
      city: [''],
      deposit: [''],
      discount: [''],
      caratsSent: [''],
      caratsRecieved: [''],
      itemRows: this.formbuilder.array([ this.createEditItem() ])
    });
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filterCity(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.cities.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  ngOnInit() {
    this.todayDate = this.formatDate(new Date());
    this.billing_date = this.formatDate(new Date());
    this.filteredCityOptions = this.addBillForm.get('city').valueChanges.pipe(
      startWith(''),
      map(value => this._filterCity(value))
    );
    this.filteredOptions = this.addBillForm.get('name').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.spinner.show();
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopId = data.shop._id;
      this.shopDetails = data.shop;
      var str = this.shopDetails.shopName;
      if(str) {
        var matches = str.match(/\b(\w)/g);
        this.shopDetails.shopName = matches.join('');
      }
      
      this.commonServ.getAll('api/merchant/specific-shop-merchants/' + this.shopId).subscribe((data: any) =>{
        this.merchants = data.merchant;
      });
      this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=1&limit=200').subscribe((data: any) =>{
        this.customers = data.data;
        this.customers.forEach(customer => {
          this.AllCustomers.push(customer.name);
          this.AllCities.push(customer.city);
        });
        this.options = Array.from(new Set(this.AllCustomers));
        this.cities = Array.from(new Set(this.AllCities));
      });
      this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId + '?page=1&limit=500').subscribe((data: any) =>{
        this.bills = data.data;
        if(this.bills.length) {
          this.bills.forEach(bill => {
            if(bill.itemRows.length) {
              bill.itemRows.forEach(item => {
                if(item.description) {
                  this.AllDescriptions.push(item.description);
                }
              });
            }
          });
        }
        this.descriptions = Array.from(new Set(this.AllDescriptions));
      });
      this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
        this.bills = data.data;
        this.pagination = data.pagination;
        this.totalBills = data.pagination.total;
        if(this.bills.length) {
          this.commonServ.getSingle('api/bill', this.bills[0]._id).subscribe((data: any) =>{
            this.bill = data.bill;
            this.selected = data.bill._id;
          });
        }
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    });
  }

  createItem(): FormGroup {
    return this.formbuilder.group({
      description: [''],
      merchant: [''],
      ctnKg: [''],
      rate: [''],
      lot: [''],
      itemAmount: ['']
    });
  }

  createEditItem(): FormGroup {
    return this.formbuilder.group({
      description: [''],
      merchant: [''],
      ctnKg: [''],
      rate: [''],
      lot: ['']
    });
  }

  addItem(): void {
    this.itemRows = this.addBillForm.get('itemRows') as FormArray;
    this.itemRows.push(this.createItem());
  }

  deleteRow(index: number) {
    this.itemRows = this.addBillForm.get('itemRows') as FormArray;
    this.itemRows.removeAt(index);
  }

  editItem(): void {
    this.itemRows = this.editBillForm.get('itemRows') as FormArray;
    this.itemRows.push(this.createEditItem());
  }

  deleteEditRow(index: number) {
    this.itemRows = this.editBillForm.get('itemRows') as FormArray;
    this.itemRows.removeAt(index);
  }

  deleteSplitRow() {
    this.itemRows = this.addBillForm.get('itemRows') as FormArray;
    while (this.itemRows.length !== 1) {
      this.itemRows.removeAt(0);
    }
  }

  onReset() {
    this.addBillForm.patchValue({
      name: '',
      city: '',
      deposit: '',
      discount: '',
      caratsSent: '',
      caratsRecieved: ''
    });
    this.deleteSplitRow();
    this.itemRows = this.addBillForm.get('itemRows') as FormArray;
    this.itemRows.at(0).patchValue({
      description: '',
      merchant: '',
      ctnKg: '',
      rate: '',
      lot: ''
    });
  }

  isActive(item) {
    return this.selected === item;
  };

  billDate(event) {
    this.fromDatePicker = true;
    this.billing_date = event.target.value;
  }

  onAddBillSubmit() {
    this.addBillForm.value.shopId = this.shopId;
    this.addBillForm.value.fromDatePicker = this.fromDatePicker;
    this.addBillForm.value.created_at = this.billing_date;
    this.addBillForm.value.totalDeposit = this.addBillForm.value.deposit;
    this.spinner.show();
    this.commonServ.post('api/bill', this.addBillForm.value).subscribe((data: any) => {
      this.toastr.success('Bill creted successfully');
      this.closebutton.nativeElement.click();
      this.fromDatePicker = false;
      this.onReset();
      this.commonServ.getSingle('api/bill', data.bill._id).subscribe((data: any) =>{
        this.bill = data.bill;
        this.selected = data.bill._id;
      });
      this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=1&limit=200').subscribe((data: any) =>{
        this.customers = data.data;
        this.customers.forEach(customer => {
          this.AllCustomers.push(customer.name);
          this.AllCities.push(customer.city);
        });
        this.options = Array.from(new Set(this.AllCustomers));
        this.cities = Array.from(new Set(this.AllCities));
      });
      this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId + '?page=1&limit=500').subscribe((data: any) =>{
        this.bills = data.data;
        if(this.bills.length) {
          this.bills.forEach(bill => {
            if(bill.itemRows.length) {
              bill.itemRows.forEach(item => {
                if(item.description) {
                  this.AllDescriptions.push(item.description);
                }
              });
            }
          });
        }
        this.descriptions = Array.from(new Set(this.AllDescriptions));
      });
      this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId  + '?page=1' + '&limit=' + this.itemPage).subscribe((data: any) =>{
        this.bills = data.data;
        this.page = 1;
        this.pagination = data.pagination;
        this.totalBills = data.pagination.total;
        this.spinner.hide();
      });
    }, (error) => {
      console.log(error);
    });
  }

  deleteBill(id, name, city) {
    if (confirm(`Sure to delete ${name} | ${city} bill`)) {
      this.spinner.show();
      this.commonServ.delete('api/bill/delete', id).subscribe((data: any) => {
        this.toastr.success('Bill deleted successfully');
        this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId  + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
          this.bills = data.data;
          this.pagination = data.pagination;
          this.totalBills = data.pagination.total;
          if(this.bills.length) {
            this.commonServ.getSingle('api/bill', this.bills[0]._id).subscribe((data: any) =>{
              this.bill = data.bill;
              this.selected = data.bill._id;
            });
          }
          this.spinner.hide();
        });
      }, (error) => {
        console.log(error);
      });
    }
  }

  getSingleBill(id) {
    this.spinner.show();
    this.commonServ.getSingle('api/bill', id).subscribe((data: any) =>{
      this.spinner.hide();
      this.bill = data.bill;
      this.editBillData = data.bill;
      if(this.editBillData) {
        this.editBillForm.patchValue({
          name: this.editBillData.customerId.name,
          city: this.editBillData.customerId.city,
          deposit: this.editBillData.deposit,
          discount: this.editBillData.discount,
          caratsSent: this.editBillData.caratsSent,
          caratsRecieved: this.editBillData.caratsRecieved
        });
      }
      this.editBillForm.setControl('itemRows', this.setExistingItemRows(this.editBillData.itemRows));
      if(data.bill) {
        this.selected = data.bill._id;
      }
    });
  }

  setExistingItemRows(itemRow): FormArray {
    const formArray = new FormArray([]);
    itemRow.forEach(item => {
      formArray.push(this.formbuilder.group({
        description: item.description,
        merchant: item.merchant,
        rate: item.rate,
        ctnKg: item.ctnKg,
        lot: item.lot
      }));
    });
    return formArray;
  }

  onEditBillSubmit() {
    this.editBillForm.value.shopId = this.shopId;
    this.editBillForm.value.prevDeposit = this.editBillData.deposit;
    this.editBillForm.value.prevTotalAmount = this.editBillData.totalAmount;
    // this.editBillForm.value.customerId = this.editBillData.customer_id;
    this.spinner.show();
    this.commonServ.update('api/bill/update', this.editBillData._id, this.editBillForm.value).subscribe((data: any) => {
      this.toastr.success('Bill updated successfully');
      this.closeeditbutton.nativeElement.click();
      this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
        this.bills = data.data;
        this.pagination = data.pagination;
        this.totalBills = data.pagination.total;
        if(this.bills.length) {
          this.commonServ.getSingle('api/bill', this.bills[0]._id).subscribe((data: any) =>{
            this.bill = data.bill;
            this.selected = data.bill._id;
          });
        }
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    }, (error) => {
      console.log(error);
    });
  }

  pageChanged(event) {
    window.scrollTo(0, 0);
    this.page = event;
    this.spinner.show();
    this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage + '&city=' + this.selected_city + '&cust_id=' + this.selected_customer + '&created_at=' + this.selected_date).subscribe((data: any) =>{
      this.bills = data.data;
      if(this.bills.length) {
      this.selected = this.bills[0]._id;
      this.commonServ.getSingle('api/bill', this.bills[0]._id).subscribe((data: any) =>{
          this.bill = data.bill;
          this.selected = data.bill._id;
        });
      }
      this.spinner.hide();
      }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }

  timespan(age) {
    if(age == 'new'){
      this.page = 1;
    } else {
      this.page = Math.ceil(this.totalBills / this.itemPage);
    }
    this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
      this.bills = data.data;
      this.selected = data.data[0]._id;
      this.pagination = data.pagination;
      this.totalBills = data.pagination.total;
      if(this.bills.length) {
        this.commonServ.getSingle('api/bill', this.bills[0]._id).subscribe((data: any) =>{
          this.bill = data.bill;
          this.selected = data.bill._id;
        });
      }
      this.spinner.hide();
    }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }
  
  customerFilter(cust_id) {
    this.spinner.show();
    this.selected_customer = cust_id;
    this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId  + '?page=1' + '&limit=' + this.itemPage + '&cust_id=' + this.selected_customer).subscribe((data: any) =>{
      this.bills = data.data;
      this.page = 1;
      this.pagination = data.pagination;
      this.totalBills = data.pagination.total;
      this.spinner.hide();
    });
  }

  dateFilter(created_at) {
    this.spinner.show();
    this.selected_date = created_at;
    this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId  + '?page=1' + '&limit=' + this.itemPage + '&created_at=' + this.selected_date).subscribe((data: any) =>{
      this.bills = data.data;
      this.page = 1;
      this.pagination = data.pagination;
      this.totalBills = data.pagination.total;
      this.spinner.hide();
    });
  }

  cityFilter(name) {
    this.spinner.show();
    this.selected_city = name;
    this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId  + '?page=1' + '&limit=' + this.itemPage + '&city=' + this.selected_city).subscribe((data: any) =>{
      this.bills = data.data;
      this.page = 1;
      this.pagination = data.pagination;
      this.totalBills = data.pagination.total;
      this.spinner.hide();
    });
  }

  salesReportForSpecificDate(event) {
    this.spinner.show();
    this.selected_date = event.target.value;
    this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId  + '?page=1' + '&limit=' + this.itemPage + '&created_at=' + this.selected_date).subscribe((data: any) =>{
      this.bills = data.data;
      this.page = 1;
      this.pagination = data.pagination;
      this.totalBills = data.pagination.total;
      this.spinner.hide();
    });
  }

  allBill() {
    this.spinner.show();
    this.page = 1;
    this.selected_city = '';
    this.selected_customer = '';
    this.selected_date = '';
    this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=1&limit=100').subscribe((data: any) =>{
      this.customers = data.data;
    });
    this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
      this.bills = data.data;
      this.pagination = data.pagination;
      this.totalBills = data.pagination.total;
      if(this.bills.length) {
        this.commonServ.getSingle('api/bill', this.bills[0]._id).subscribe((data: any) =>{
          this.bill = data.bill;
          this.selected = data.bill._id;
        });
      }
      this.spinner.hide();
    }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }

  printDiv() {
    var win = window.open('','printwindow');
    // win.document.write('<html><head><title>Print it!</title><link rel="stylesheet" type="text/css" href="styles.css"></head><body>');
    win.document.write($("#printableBillArea").html());
    // win.document.write('</body></html>');
    win.print();
    // win.close();
  }

  getDescription(value) { 
    var n = this.descriptions.length;
    document.getElementById('datalist').innerHTML = ''; 
    var l=value.length; 
    for (var i = 0; i<n; i++) { 
      if(((this.descriptions[i].toLowerCase()).indexOf(value.toLowerCase()))>-1) 
      { 
        var node = document.createElement("option"); 
        var val = document.createTextNode(this.descriptions[i]); 
        node.appendChild(val); 
        document.getElementById("datalist").appendChild(node); 
      } 
    } 
  } 

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  }
}
