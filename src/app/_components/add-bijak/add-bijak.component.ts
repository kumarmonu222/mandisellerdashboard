import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as jwt_decode from 'jwt-decode';
import * as $ from "jquery";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-add-bijak',
  templateUrl: './add-bijak.component.html',
  styleUrls: ['./add-bijak.component.scss']
})
export class AddBijakComponent implements OnInit {
  addBijakForm: FormGroup;
  itemRows: FormArray;
  submitted: boolean;
  shopDetails: any;
  itemSno: number = 1;
  options: string[] = [];
  cities: string[] = [];
  filteredCityOptions: Observable<string[]>;
  filteredOptions: Observable<string[]>;
  AllCities: any = [];
  AllMerchants: any = [];
  shopId: any;
  AllDescriptions: any = [];
  descriptions: any = [];

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private router:Router,
    private spinner: NgxSpinnerService
  ) { 
    this.addBijakForm = this.formbuilder.group({ 
      no: [''],
      city: new FormControl(name, Validators.required),
      ms: new FormControl(name, Validators.required),
      dateOfReciept: [''],
      dispatchDate: [''],
      truckNo: [''],
      truckFreight: [''],
      railFreight: [''],
      stationMandiExp: [''],
      unloading: [''],
      postage: [''],
      phone: [''],
      coldStorage: [''],
      loading: [''],
      charity: [''],
      itemRows: this.formbuilder.array([ this.createItem() ])
    });
  }

  createItem(): FormGroup {
    return this.formbuilder.group({
      sno: this.itemSno,
      description: '',
      ctnKg: '',
      rate: '',
      itemAmount: ''
    });
  }

  addItem(i): void {
    this.itemSno += i;
    this.itemRows = this.addBijakForm.get('itemRows') as FormArray;
    this.itemRows.push(this.createItem());
  }

  deleteRow(index: number) {
    this.itemRows = this.addBijakForm.get('itemRows') as FormArray;
    this.itemRows.removeAt(index);
  }

  private _filterCity(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.cities.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit() {
    this.filteredCityOptions = this.addBijakForm.get('city').valueChanges.pipe(
      startWith(''),
      map(value => this._filterCity(value))
    );
    this.filteredOptions = this.addBijakForm.get('ms').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopDetails = data.shop;
      this.shopId = data.shop._id; 
      this.commonServ.getAll('api/bijak/specific-shop-bijaks/' + this.shopId + '?page=1&limit=200').subscribe((data: any) =>{
        if(data.data.length) {
          data.data.forEach(bijak => {
            if(bijak.itemRows.length) {
              bijak.itemRows.forEach(item => {
                if(item.description) {
                  this.AllDescriptions.push(item.description);
                }
              });
            }
          });
        }
        this.descriptions = Array.from(new Set(this.AllDescriptions));
      });
      this.commonServ.getAll('api/merchant/specific-shop-merchants/' + this.shopId).subscribe((data: any) =>{
        data.merchant.forEach(merchant => {
          this.AllMerchants.push(merchant.name);
          this.AllCities.push(merchant.city);
        });
        this.options = Array.from(new Set(this.AllMerchants));
        this.cities = Array.from(new Set(this.AllCities));
      });
    });
  }

  allBijak() {
    this.router.navigateByUrl('dashboard/bijak/all-bijak');
  }

  onReset() {
    this.submitted = false;
    this.addBijakForm.reset();
  }

  onAddBijakSubmit() {
    this.submitted = true;
    this.spinner.show();
    this.addBijakForm.value.shopId = this.shopDetails._id;
    this.addBijakForm.value.ms = this.addBijakForm.value.ms.toLowerCase();
    this.addBijakForm.value.city = this.addBijakForm.value.city.toLowerCase();
    this.commonServ.post('api/bijak', this.addBijakForm.value).subscribe((data: any) => {
      this.toastr.success('Bijak created successfully');
      this.spinner.hide();
      this.router.navigate(['dashboard/bijak/all-bijak']);
    }, (error) => {
      console.log(error);
    });
  }

  getDescription(value) { 
    var n = this.descriptions.length;
    document.getElementById('datalist').innerHTML = ''; 
    var l=value.length; 
    for (var i = 0; i<n; i++) { 
      if(((this.descriptions[i].toLowerCase()).indexOf(value.toLowerCase()))>-1) 
      { 
        var node = document.createElement("option"); 
        var val = document.createTextNode(this.descriptions[i]); 
        node.appendChild(val); 
        document.getElementById("datalist").appendChild(node); 
      } 
    } 
  } 
}