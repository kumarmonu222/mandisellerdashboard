import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBijakComponent } from './add-bijak.component';

describe('AddBijakComponent', () => {
  let component: AddBijakComponent;
  let fixture: ComponentFixture<AddBijakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBijakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBijakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
