import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as jwt_decode from 'jwt-decode';
import * as $ from "jquery";
import { NgxSpinnerService } from "ngx-spinner";
import { DomSanitizer } from "@angular/platform-browser"; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-bijak',
  templateUrl: './all-bijak.component.html',
  styleUrls: ['./all-bijak.component.scss']
})
export class AllBijakComponent implements OnInit {
  products: any;
  shopId: any;
  bijaks: any;
  singleBijak: any;
  selected: any;
  logoImage: string;
  page: number = 1;
  itemPage: number = 10;
  pagination: any;
  totalBijaks: any;
  shop: any;
  bijakSno: number;
  smallDevice: boolean;

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private spinner: NgxSpinnerService,
    public sanitizer: DomSanitizer,
    public router: Router
  ) { }

  ngOnInit() {
    if (window.outerWidth <= 768) {
      this.smallDevice = true;
    } else {
      this.smallDevice = false;
    }
    this.spinner.show();
    this.logoImage = localStorage.getItem('logoImage');
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopId = data.shop._id;
      this.shop = data.shop;
      var str = this.shop.shopName;
      if(str) {
        var matches = str.match(/\b(\w)/g);
        this.shop.shortShopName = matches.join('');
      }
      this.commonServ.getAll('api/bijak/specific-shop-bijaks/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
        this.bijaks = data.data;
        this.pagination = data.pagination;
        this.totalBijaks = data.pagination.total;
        if(this.bijaks.length) {
          this.commonServ.getSingle('api/bijak/', this.bijaks[0]._id).subscribe((data: any) => {
            this.singleBijak = data.bijak;
            this.singleBijak.bijakNo = data.bijak._id.substr(7,25);
            this.selected = data.bijak._id;
          });
        }
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    });
  }

  getBijak(bijakId) {
    this.spinner.show();
    this.commonServ.getSingle('api/bijak/', bijakId).subscribe((data: any) => {
      this.singleBijak = data.bijak;
      this.singleBijak.bijakNo = data.bijak._id.substr(7,25);
      this.spinner.hide();
      this.selected = data.bijak._id;
    });
  }

  isActive(item) {
    return this.selected === item;
  };
  
  deleteBijak(id) {
    if (confirm("Sure to delete bijak")) {
      this.spinner.show();
      this.commonServ.delete('api/bijak/delete', id).subscribe((data: any) => {
        this.toastr.success('Bijak deleted successfujaky');
        this.commonServ.getAll('api/bijak/specific-shop-bijaks/' + this.shopId  + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
          this.bijaks = data.data;
          this.pagination = data.pagination;
          this.totalBijaks = data.pagination.total;
          if(this.bijaks.length) {
            this.commonServ.getSingle('api/bijak', this.bijaks[0]._id).subscribe((data: any) =>{
              this.singleBijak = data.bijak;
              this.selected = data.bijak._id;
            });
          }
          this.spinner.hide();
        });
      }, (error) => {
        console.log(error);
      });
    }
  }

  pageChanged(event) {
    window.scrollTo(0, 0);
    this.page = event;
    this.spinner.show();
    this.commonServ.getAll('api/bijak/specific-shop-bijaks/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
      this.bijaks = data.data;
      this.pagination = data.pagination;
      this.totalBijaks = data.pagination.total;
      if(this.bijaks.length) {
        this.commonServ.getSingle('api/bijak', this.bijaks[0]._id).subscribe((data: any) =>{
          this.singleBijak = data.bijak;
          this.selected = data.bijak._id;
        });
      }
      this.spinner.hide();
      }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }

  addBijak() {
    this.router.navigateByUrl('dashboard/bijak/add-bijak');
  }

  allBijak() {
    this.spinner.show();
    this.page = 1;
    this.commonServ.getAll('api/bijak/specific-shop-bijaks/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
      this.bijaks = data.data;
      this.pagination = data.pagination;
      this.totalBijaks = data.pagination.total;
      if(this.bijaks.length) {
        this.commonServ.getSingle('api/bijak/', this.bijaks[0]._id).subscribe((data: any) => {
          this.singleBijak = data.bijak;
          this.selected = data.bijak._id;
        });
      }
      this.spinner.hide();
    }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }

  printDiv() {
    console.log(this.smallDevice);
    var win = window.open('','printwindow');
    // win.document.write('<html><head><title>Print it!</title><link rel="stylesheet" type="text/css" href="styles.css"></head><body>');
    win.document.write($("#printableBijakArea").html());
    // win.document.write('</body></html>');
    win.print();
    // win.close();
  }
}