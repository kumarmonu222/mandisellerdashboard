import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import * as $ from 'jquery';

@Component({
  selector: 'app-single-customer',
  templateUrl: './single-customer.component.html',
  styleUrls: ['./single-customer.component.scss']
})
export class SingleCustomerComponent implements OnInit {
  bills: any;
  cust_name: string;
  cust_phone: string;
  cust_city: string;
  smallDevice: boolean;

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    if (window.outerWidth <= 768) {
      this.smallDevice = true;
    } else {
      this.smallDevice = false;
    }
    this.spinner.show();
    this.getCustomer();
    this.commonServ.getAll('api/bill/customer/' + localStorage.getItem('cust_id')).subscribe((data: any) =>{
      this.bills = data.bill;
      this.spinner.hide();
    }, (error:any) => {
      console.log(error);
    });
  }

  getCustomer() {
    this.cust_name = window.localStorage.getItem('cust_name');
    this.cust_city = window.localStorage.getItem('cust_city');
    this.cust_phone = window.localStorage.getItem('cust_phone');
  }

  printDiv() {
    var win = window.open('','printwindow');
    // win.document.write('<html><head><title>Print it!</title><link rel="stylesheet" type="text/css" href="styles.css"></head><body>');
    win.document.write($("#printableTransactionArea").html());
    // win.document.write('</body></html>');
    win.print();
    // win.close();
  }
}
