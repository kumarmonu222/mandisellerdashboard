import { Component, OnInit } from '@angular/core';
// import * as $ from 'jquery';
declare var $: any;
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import * as jwt_decode from 'jwt-decode';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NavbarService } from 'src/app/_services/navbar.service';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      share() 
    );

  constructor(
    private formbuilder: FormBuilder, 
    private router: Router, 
    public toastr: ToastrService,
    private authService: AuthService,
    public navbarServ: NavbarService,
    private commonServ: CommonServicesService,
    private breakpointObserver: BreakpointObserver, 
    private titleService: Title
  ) { 
    
  }

  ngOnInit() {
   
  }

  notification() {
    console.log('notification');
  }

  logout() {
    this.navbarServ.logout();
    this.router.navigate(['/']);
  }
}