import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-sell-report',
  templateUrl: './sell-report.component.html',
  styleUrls: ['./sell-report.component.scss']
})
export class SellReportComponent implements OnInit {
  shopId: any;
  salesReportData: any;
  todayDate: any;
  bills: any;
  totalSells: any = [];
  selected_date: any;
  customer_name: any;
  customer_city: any;

  constructor(
    private commonServ: CommonServicesService,
    public toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private router:Router,
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.selected_date = this.formatDate(new Date());
    this.todayDate = this.formatDate(new Date());
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopId = data.shop._id;
      this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId + '?page=1' + '&limit=250&created_at=' + this.selected_date).subscribe((data: any) => {
        this.bills = data.data;
        if(this.bills.length) {
          this.bills.forEach(bill => {
            this.customer_name = bill.customer[0].name;
            this.customer_city = bill.customer[0].city;
            if(bill.itemRows.length) {
              bill.itemRows.forEach(item => {
                this.totalSells.push({
                  customerName: this.customer_name, 
                  customerCity: this.customer_city,
                  item: item
                });
              });
            }
          });
        }
        this.totalSells.sort((a, b) => {
          return a.item.lot - b.item.lot;
        });
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    });
  }

  salesReportForSpecificDate(event) {
    this.spinner.show();
    this.totalSells = [];
    this.selected_date = event.target.value;
    console.log(this.selected_date);
    this.commonServ.getAll('api/bill/specific-shop-bills/' + this.shopId + '?page=1' + '&limit=250&created_at=' + this.selected_date).subscribe((data: any) => {
      this.bills = data.data;
      if(this.bills.length) {
        this.bills.forEach(bill => {
          this.customer_name = bill.customer[0].name;
          this.customer_city = bill.customer[0].city;
          if(bill.itemRows.length) {
            bill.itemRows.forEach(item => {
              this.totalSells.push({
                customerName: this.customer_name, 
                customerCity: this.customer_city,
                item: item
              });
            });
          }
        });
      }
      this.spinner.hide();
    }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  } 
}
