import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBijakComponent } from './edit-bijak.component';

describe('EditBijakComponent', () => {
  let component: EditBijakComponent;
  let fixture: ComponentFixture<EditBijakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBijakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBijakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
