import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import * as jwt_decode from 'jwt-decode';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  @ViewChild('closebutton', { static: true }) closebutton;
  @ViewChild('closeeditbutton', { static: true }) closeeditbutton;
  page: number = 1;
  itemPage: number = 10;
  shopId: any;
  customers: any;
  addCustomerForm: FormGroup;
  editCustomerForm: FormGroup;
  searchCustomerForm: FormGroup;
  submitted: boolean;
  customer: any;
  pagination: any;
  totalCustomers: any;
  selectedCity: string = '';
  options: string[] = [];
  cities: string[] = [];
  filteredCityOptions: Observable<string[]>;
  filteredOptions: Observable<string[]>;
  AllCities: any = [];
  AllCustomers: any = [];
  selectedCustomer: string = '';

  constructor(
    private commonServ: CommonServicesService,
    private formbuilder: FormBuilder, 
    public toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { 
    this.addCustomerForm = this.formbuilder.group({ 
      name: ['', [Validators.required]],
      city: new FormControl(name, Validators.required),
      phone: ['', [Validators.required]],
    });

    this.editCustomerForm = this.formbuilder.group({ 
      phone: [''],
    });

    this.searchCustomerForm = this.formbuilder.group({ 
      customer: new FormControl(name)
    });
  }

  private _filterCity(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.cities.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit() {
    this.spinner.show();
    this.filteredCityOptions = this.addCustomerForm.get('city').valueChanges.pipe(
      startWith(''),
      map(value => this._filterCity(value))
    );
    this.filteredOptions = this.searchCustomerForm.get('customer').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopId = data.shop._id;
      this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=1' + '&limit=200').subscribe((data: any) =>{
        data.data.forEach(customer => {
          this.AllCustomers.push(customer.name);
          this.AllCities.push(customer.city);
        });
        this.options = Array.from(new Set(this.AllCustomers));
        this.cities = Array.from(new Set(this.AllCities));
      });
      this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
        this.customers = data.data;
        this.pagination = data.pagination;
        this.totalCustomers = data.pagination.total;
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    });
  }

  onReset() {
    this.submitted = false;
    this.addCustomerForm.reset();
    this.editCustomerForm.reset();
  }

  onAddCustomerSubmit() {
    this.submitted = true;
    this.addCustomerForm.value.shopId = this.shopId;
    this.spinner.show();
    this.commonServ.post('api/customer', this.addCustomerForm.value).subscribe((data: any) => {
      this.toastr.success('Customer creted successfully');
      this.closebutton.nativeElement.click();
      this.selectedCity = '';
      this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
        this.customers = data.data;
        this.pagination = data.pagination;
        this.totalCustomers = data.pagination.total;
        this.customers.forEach(customer => {
          this.AllCities.push(customer.city);
        });
        this.cities = Array.from(new Set(this.AllCities));
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    }, (error) => {
      console.log(error);
    });
  }

  deleteCustomer(id, name, city) {
    if (confirm(`Sure to delete ${name} | ${city}`)) {
      this.spinner.show();
      this.commonServ.delete('api/customer/delete', id).subscribe((data: any) => {
        this.toastr.success('Customer deleted successfully');
        this.selectedCity = '';
        this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
          this.customers = data.data;
          this.pagination = data.pagination;
          this.totalCustomers = data.pagination.total;
          this.spinner.hide();
        }, (error:any) => {
          if(error.error.message === "No Entries found") {
            this.spinner.hide();
          }
        });
      }, (error) => {
        console.log(error);
      });
    }
  }

  getSingleCustomer(id) {
    this.spinner.show();
    this.commonServ.getSingle('api/customer', id).subscribe((data: any) =>{
      this.spinner.hide();
      this.customer = data.customer;
    });
  }

  onEditCustomerSubmit(id) {
    this.submitted = true;
    this.editCustomerForm.value.shopId = this.shopId;
    this.spinner.show();
    this.commonServ.update('api/customer/update', id, this.editCustomerForm.value).subscribe((data: any) => {
      this.toastr.success('Customer updated successfully');
      this.closeeditbutton.nativeElement.click();
      this.selectedCity = '';
      this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
        this.customers = data.data;
        this.pagination = data.pagination;
        this.totalCustomers = data.pagination.total;
        this.spinner.hide();
      }, (error:any) => {
        if(error.error.message === "No Entries found") {
          this.spinner.hide();
        }
      });
    }, (error) => {
      console.log(error);
    });
  }

  setCustomer(id, name, city, phone) {
    localStorage.setItem('cust_id', id);
    localStorage.setItem('cust_name', name);
    localStorage.setItem('cust_city', city);
    if(typeof phone !== 'undefined') {
      localStorage.setItem('cust_phone', phone);
    }
  }

  pageChanged(event) {
    window.scrollTo(0, 0);
    this.page = event;
    this.spinner.show();
    this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage + '&city=' + this.selectedCity + '&customer=' + this.selectedCustomer).subscribe((data: any) =>{
      this.customers = data.data;
      this.pagination = data.pagination;
      this.totalCustomers = data.pagination.total;
      this.spinner.hide();
    }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }

  timespan(age) {
    if(age == 'new'){
      this.page = 1;
    } else {
      this.page = Math.ceil(this.totalCustomers / this.itemPage);
    }
    this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
      this.customers = data.data;
      this.pagination = data.pagination;
      this.totalCustomers = data.pagination.total;
      this.spinner.hide();
    }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }

  cityFilter(name) {
    this.spinner.show();
    this.selectedCity = name;
    this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId  + '?page=1' + '&limit=' + this.itemPage + '&city=' + name).subscribe((data: any) =>{
      this.customers = data.data;
      this.page = 1;
      this.pagination = data.pagination;
      this.totalCustomers = data.pagination.total;
      this.spinner.hide();
    });
  }

  allCustomer() {
    this.spinner.show();
    this.page = 1;
    this.selectedCity = '';
    this.selectedCustomer = '';
    this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage).subscribe((data: any) =>{
      this.customers = data.data;
      this.pagination = data.pagination;
      this.totalCustomers = data.pagination.total;
      this.spinner.hide();
    }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }

  onSearchCustomerSubmit(customer) {
    this.page = 1;
    this.selectedCustomer = customer;
    this.commonServ.getAll('api/customer/specific-shop-customers/' + this.shopId + '?page=' + this.page + '&limit=' + this.itemPage + '&customer=' + this.selectedCustomer).subscribe((data: any) =>{
      this.customers = data.data;
      this.pagination = data.pagination;
      this.totalCustomers = data.pagination.total;
      this.spinner.hide();
    }, (error:any) => {
      if(error.error.message === "No Entries found") {
        this.spinner.hide();
      }
    });
  }
}
