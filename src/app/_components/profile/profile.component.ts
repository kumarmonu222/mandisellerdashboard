import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { PusherService } from 'src/app/_services/pusher.service';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  shopDetails: any;

  constructor(private formbuilder: FormBuilder, 
    private router: Router, 
    private pusherService: PusherService,
    public toastr: ToastrService,
    private commonServ: CommonServicesService) { }

  ngOnInit() {
    
  }

}
