import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { CommonServicesService } from 'src/app/_services/commonservice.service';
import { PusherService } from 'src/app/_services/pusher.service';
import * as jwt_decode from 'jwt-decode';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
  shopDetails: any;
  images: any;
  logoImage: string;
  editShopForm: FormGroup;
  @ViewChild('closeeditbutton', { static: true }) closeeditbutton;
  editShopData: any;
  
  constructor(private formbuilder: FormBuilder,
    private _http:HttpClient,
    private router: Router, 
    private pusherService: PusherService,
    public toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private commonServ: CommonServicesService) { 
      this.editShopForm = this.formbuilder.group({ 
        shopName: [''],
        shopAddress: [''],
        shopCity: [''],
        shopPhone1: [''],
        shopPhone2: [''],
      });
    }

  ngOnInit() {
    this.spinner.show();
    this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
      this.shopDetails = data.shop;
      this.editShopData = data.shop;
      this.editShopForm.patchValue({
        shopName: this.editShopData.shopName,
        shopCity: this.editShopData.shopCity,
        shopAddress: this.editShopData.shopAddress,
        shopPhone1: this.editShopData.shopPhone1,
        shopPhone2: this.editShopData.shopPhone2
      });
      this.spinner.hide();
    }); 
    // this.logoImage = localStorage.getItem('logoImage');
  }

  selectImage(event) {
    if(event.target.files.length > 0) {
      const file = event.target.files[0];
      this.images = file;
    }
  }

  uploadLogoImage() {
    this.spinner.show();
    const formData = new FormData();
    formData.append('logoImage', this.images);
    this._http.post<any>(`${environment.baseUrl}api/user/upload-logo-image`, formData).subscribe((data) => {
      // localStorage.setItem('logoImage', data.user.logoImage);
      // this.ngOnInit();
    }, (error) => {
      console.log(error);
    });
  }

  onReset() {
    this.editShopForm.reset();
  }

  onEditShopSubmit(id) {
    this.spinner.show();
    this.commonServ.update('api/shop/update_shop_details', id, this.editShopForm.value).subscribe((data: any) => {
      this.toastr.success('Shop updated successfully');
      this.closeeditbutton.nativeElement.click();
      this.commonServ.getSingle('api/shop/detail', jwt_decode(localStorage.getItem('token')).id).subscribe((data:any) => {
        this.shopDetails = data.shop;
        this.spinner.hide();
      }); 
    }, (error) => {
      console.log(error);
    });
  }
}
