import { Injectable } from '@angular/core';
declare const Pusher: any;
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
  
@Injectable({
  providedIn: 'root'
})
export class PusherService {
  pusher: any;
  channel: any;
  constructor(private http: HttpClient) { 
    this.pusher = new Pusher(environment.pusher.key, {
      cluster: environment.pusher.cluster,
      encrypted: true
    });
    this.channel = this.pusher.subscribe('notifications');
  }

  userUpdate() {
    this.http.put('http://localhost:3000/api/user/update', {"id":"5e4bac4cb693c02f34c2c570", "firstName":"abcsdsas"})
    .subscribe(data => {
      console.log(data);
    });
  }
}
